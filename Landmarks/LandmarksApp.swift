//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Ferguson on 12/28/20.
//

import SwiftUI

@main
struct LandmarksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
