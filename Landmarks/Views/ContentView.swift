//
//  ContentView.swift
//  Landmarks
//
//  Created by Ferguson on 12/28/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
       LandmarkList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
